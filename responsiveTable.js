$(document).ready(function() {
	var table = $('table');
	
	function respinsiveTable(table, origTable) {
		var tableHeaderRow = table.find('tr:first');
		var tableHeaders = tableHeaderRow.find('th');
		var tableDataRows = table.find('tr:not(:first-child)');
		var tbody = table.find('tbody');
		
		//Set up new table
		table.addClass('responsiveTable');
		tbody.empty();
		
		//For each row in table, create new table with all headings + relevant data
		tableDataRows.each(function(index) {
		
			//Create new table 
			tbody.append('<table class="innerTable-' + index +' responsiveRow"><tbody></tbody></table>');
			var innerTable = tbody.find('.innerTable-' + index + ' tbody');
			
			//Get appropriate data from the current row
			var rowData = $(this).find('td');
			
			//for each header, create a row with this heading + corresponding data 
			tableHeaders.each(function(index) {			
				innerTable.append('<tr class="row-' + index +  '"></tr>');
				var newRow = innerTable.find('tr.row-' + index);
				
				newRow.append('<td class="left">' + $(this).html() + '</td>');
				newRow.append('<td class="right">' + rowData.eq(index).html() + '</td>');
			});
		});
		
		table.find('table:even').addClass("even");
		table.find('table:last').addClass("last");
		
		origTable.after(table);
		origTable.addClass('fullTable');
	}
	
	table.each(function(index) {
		var $this = $(this);
		var narrowTable = $this.clone();
		
		respinsiveTable(narrowTable, $this);
	});
	
});
